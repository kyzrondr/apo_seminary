#ifndef _FRAME_BUFFER_H_
#define _FRAME_BUFFER_H_

#include <stdio.h>
#include <stdlib.h>
#include "mzapo_parlcd.h"
#include "font_types.h"

#define WIDTH 480
#define HEIGHT 320

/*
 * Allocates the frame buffer and returns it.
 */
unsigned short *init_frame_buffer();

/*
 * Draws a pixel of a given color to the frame buffer.
 */
void draw_pixel(unsigned short *fb, int x, int y, unsigned short color);

/*
 * Calls draw pixel for given scale. 
 */
void draw_pixel_scaled(int x, int y, int scale, unsigned short color);

/*
 * Paints the background with DEFAULT color (black). 
 */
void draw_background(unsigned short *fb);

/*
 * Saves the frame buffer to the memery adress of the LCD display. 
 */
void fb_to_LCD(unsigned short *fb, unsigned char *parlcd_mem_base);

/*
 * Returns the width of a given char. 
 */
int char_width(font_descriptor_t *fdes, int ch);

/*
 * Draws char on the frame buffer with given coordinates (x, y), scale and color. 
 */
void draw_char(unsigned short *fb,  int x, int y, int scale, char ch, unsigned short color);

/*
 * Calls draw pixel for given x, y, height, width and color. Draws a block/line of color. 
 */
void draw_line(unsigned short *fb, int x, int y, int height, int width,  unsigned short color);

/*
 * Draws the border of the game. 
 */
void draw_border(unsigned short *fb);

#endif