#ifndef _SNAKE_STRUCT_H_
#define _SNAKE_STRUCT_H_

typedef struct grid_block
{
  int x;
  int y;
} grid_block;

typedef struct snake
{
  grid_block head;
  grid_block *body;
  unsigned int length;
  unsigned short color;
  unsigned char direction;
} snake;

#endif
