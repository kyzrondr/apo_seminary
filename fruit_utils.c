#include "fruit_utils.h"

void spawn_fruit(snake* sn, grid_block* fruit, int width, int height)
{
  bool found_good_coords = false;

  while(!found_good_coords){
    fruit->x = (rand() % ((width/GRID_SIZE) - 1) * GRID_SIZE) + BORDER_OFFSET;
    fruit->y = (rand() % ((height/GRID_SIZE) - 1) * GRID_SIZE) + BORDER_OFFSET;
    found_good_coords = true;

    if (check_block_collision(*fruit, sn->head)){
      found_good_coords = false;
      continue;
    }

    for (int i = 0; i < sn->length; i++){
      if (check_block_collision(*fruit, sn->body[i])){
        found_good_coords = false;
        break;
      }
    }
  }
}

bool check_block_collision(grid_block block1, grid_block block2){
  return block1.x == block2.x && block1.y == block2.y;
}

void remove_fruit(grid_block* fruit, int width, int height){
  fruit->x = width - fruit->x;
  fruit->y = height/2 < fruit->y ? OUT_OF_BOUNDS : height - OUT_OF_BOUNDS;
}

