#ifndef _SNAKE_UTILS_H_
#define _SNAKE_UTILS_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "mzapo_regs.h"
#include "frame_buffer.h"
#include "fruit_utils.h"
#include "snake_struct.h"

#define MAX_SNAKE_LENGTH 400
#define START_X GRID_SIZE * 5 + 10
#define START_Y GRID_SIZE * 6 + 10

#define HARD 2
#define NORMAL 3

/*
 * Used for the snake direction.
 */
enum{up, left, down, right};

/*
 * Sets up a snake and returns it.
 */
snake *init_snake(int color);

/*
 * Draws a grid block on the frame buffer.
 */
void draw_grid_block(unsigned short *fb, grid_block part, unsigned short color);

/*
 * Frees the snake and it's body
 */
void free_snake(snake *sn);

/*
 * Moves the snakes head based on it's direction.
 */
void move_snake_head(snake *sn);

/*
 * Moves the snake.
 */
void move_snake(snake *sn);

/*
 * Lenghthens the snake by one body.
 */
void create_snake_body(snake *sn);

/*
 * Handles the input from the player and changes the direction of the snake.
 */
void handle_snake_input(snake *sn, char input);

/*
 * Draws the snake head and body to the frame buffer
 */
void draw_snake(snake *sn, unsigned short int *fb);

/*
 * Checks if the snake has collided with the borders or itself.
 */
bool check_collision(snake* sn);

/*
 * Resets the snakes position and body lenght. Used for the AI snake.
 */
void reset_snake(snake* sn);

/*
 * Generates a direction based the fruit position.
 * AI goes in X direction of the fruit then in the Y direction
 * and because the screen has bigger width than heaight the AI 
 * has more time and can live longer. 
 */
char get_AI_input(int fruit_X, int fruit_Y, snake* sn);

/*
 * Handles the input (player and AI), moves the snake if snake counter is equal
 * or bigger than the difficulty, fruit spawning, LED line, RGB diodes, fruit
 * eating and snake collision. 
 */
int handle_snake(snake* sn, grid_block* fruit, unsigned int* val_line,
                 int* snake_delay_counter, int difficulty, char input,
                 unsigned char* mem_base);

/*
 * Draws the background, borders, snake and fruit.
 */
void draw_snake_frame(unsigned short int *fb, snake* sn, grid_block fruit);
#endif
