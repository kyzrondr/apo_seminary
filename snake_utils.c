#include "snake_utils.h"

// Input posibilities for the AI snake
char INPUT[] = {'w', 'a', 's', 'd'};

snake *init_snake(int color)
{
  snake *temp_snake = (snake *)malloc(sizeof(snake));
  if (!temp_snake){
    fprintf(stderr, "Can't allocate snake!\n");
    exit(-1);
  }
  temp_snake->body = (grid_block *)malloc(sizeof(grid_block) * MAX_SNAKE_LENGTH);
  if (!temp_snake->body){
    fprintf(stderr, "Can't allocate snake body!\n");
    exit(-1);
  }
  temp_snake->head.x = START_X;
  temp_snake->head.y = START_Y;
  temp_snake->color = color;
  temp_snake->direction = right;
  temp_snake->length = 0;
  grid_block body1;
  body1.x = START_X - GRID_SIZE;
  body1.y = START_Y;
  temp_snake->body[temp_snake->length] = body1;
  temp_snake->length++;

  create_snake_body(temp_snake);
  temp_snake->body[1].x -= GRID_SIZE;

  return temp_snake;
}

void draw_grid_block(unsigned short *fb, grid_block part, unsigned short color)
{
  for (int j = 0; j < GRID_SIZE; j++){
    for (int i = 0; i < GRID_SIZE; i++){
      draw_pixel(fb, i + part.x, j + part.y, color);
    }
  }
}

void free_snake(snake *sn)
{
  if (sn->body){
    free(sn->body);
  }
  if (sn){
    free(sn);
  }
}

void move_snake_head(snake *sn)
{
  if (sn->direction == up) sn->head.y -= GRID_SIZE;
  else if (sn->direction == right) sn->head.x += GRID_SIZE;
  else if (sn->direction == down) sn->head.y += GRID_SIZE;
  else if (sn->direction == left) sn->head.x -= GRID_SIZE;
}

void move_snake(snake *sn)
{
  int prev_x = sn->head.x;
  int prev_y = sn->head.y;
  int curr_x, curr_y;

  move_snake_head(sn);
  for (int i = 0; i < sn->length; i++){
    curr_x = sn->body[i].x;
    curr_y = sn->body[i].y;
    sn->body[i].x = prev_x;
    sn->body[i].y = prev_y;
    prev_x = curr_x;
    prev_y = curr_y;
  }
}

void create_snake_body(snake *sn)
{
  if (sn->length < MAX_SNAKE_LENGTH){
    sn->body[sn->length].x = sn->body[sn->length-1].x;
    sn->body[sn->length].y = sn->body[sn->length-1].y;
    sn->length++;
  } else {
    printf("Snake is already max size!\n");
  }
}

void handle_snake_input(snake *sn, char input)
{
  if (input == 'd' && sn->direction != left) sn->direction = right;
  else if (input == 'a' && sn->direction != right) sn->direction = left;
  else if (input == 'w' && sn->direction != down) sn->direction = up;
  else if (input == 's' && sn->direction != up) sn->direction = down;
}

void draw_snake(snake *sn, unsigned short int *fb)
{
  draw_grid_block(fb, sn->head, sn->color);
  for (int k = 0; k < sn->length; k++){
    draw_grid_block(fb, sn->body[k], sn->color);
  }
}

bool check_collision(snake* sn)
{
  // Checks collision with the border
  if (sn->head.x >= WIDTH - BORDER_OFFSET || sn->head.x < BORDER_OFFSET ||
      sn->head.y >= HEIGHT- BORDER_OFFSET || sn->head.y < BORDER_OFFSET){
    return true;
  }

  // Checks collision with the snake itself
  for (int i = 0; i < sn->length; i++){
    if (check_block_collision(sn->head, sn->body[i])){
      return true;
    }
  }
  return false;
}

void reset_snake(snake* sn)
{
  sn->direction = rand() % 4;
  sn->head.x = START_X;
  sn->head.y = START_Y;
  sn->length = 0;
  create_snake_body(sn);
  create_snake_body(sn);
}

char get_AI_input(int fruit_X, int fruit_Y, snake* sn){
  int random;
  char ret;
  if (fruit_X > sn->head.x && sn->direction != left){
    ret = INPUT[right];
  }
  else if (fruit_X < sn->head.x && sn->direction != right){
    ret = INPUT[left];
  }
  else if (fruit_Y > sn->head.y && sn->direction != up){
    ret = INPUT[down];
  }
  else if (fruit_Y < sn->head.y && sn->direction != down){
    ret = INPUT[up];
  } else {
    // Generates a random direction that doesn't kill the snake
    random = (rand() % 4);
    ret = ((random + sn->direction) % 2) == 1 ? INPUT[(random+1)%4] : INPUT[random];
  }
  return ret;
}

int handle_snake(snake* sn, grid_block* fruit, unsigned int* val_line,
                int* snake_delay_counter, int difficulty, char input,
                unsigned char* mem_base)
  {
  handle_snake_input(sn, input);

  // Handles moving the snake based on difficulty
  if (*snake_delay_counter >= difficulty){
    move_snake(sn);
    *snake_delay_counter = 0;
    *(volatile unsigned int*)(mem_base + SPILED_REG_LED_RGB1_o) = HEX_BLACK;
    *(volatile unsigned int*)(mem_base + SPILED_REG_LED_RGB2_o) = HEX_BLACK;
  }
  (*snake_delay_counter)++;

  // Handels fruit spawning and LED line
  if (fruit->y <= BORDER_OFFSET || fruit->y >= HEIGHT - BORDER_OFFSET ||
      fruit->x <= BORDER_OFFSET || fruit->x >= HEIGHT - BORDER_OFFSET){
    if (*val_line >= UINT32_MAX >> 1){
      spawn_fruit(sn, fruit, WIDTH, HEIGHT);
    }
    *val_line <<= 1;
  }

  // Handles fruit eating
  if (check_block_collision(*fruit, sn->head)){
    create_snake_body(sn);
    remove_fruit(fruit, WIDTH, HEIGHT);
    *val_line = 1;
    *(volatile unsigned int*)(mem_base + SPILED_REG_LED_RGB1_o) = FRUIT_COLOR_SIX_DIGIT;
    *(volatile unsigned int*)(mem_base + SPILED_REG_LED_RGB2_o) = FRUIT_COLOR_SIX_DIGIT;
  }
  return check_collision(sn);
}

void draw_snake_frame(unsigned short int *fb, snake* sn, grid_block fruit)
{
  draw_background(fb);
  draw_grid_block(fb, fruit, FRUIT_COLOR);
  draw_snake(sn, fb);
  draw_border(fb);
}
