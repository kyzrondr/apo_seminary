#ifndef _MENU_H_
#define _MENU_H_

#include "snake_utils.h"

/*
 * Handles the players input for menu such as Start game, Quit game,
 * color selection and difficulty selection.
 */
bool handle_menu_input(char input, bool *play_game, bool *exit_game,
                        int* DIFFICULTY, int* idx, char* color);

/*
 * Draws the GUI of the menu (letters) to the frame buffer.
 */
void draw_menu_ui(unsigned short int *fb, char color[4]);

/*
 * Draws the background, border, fruit, snake and menu UI.
 */
void create_menu_frame(unsigned short int *fb, snake* sn, grid_block fruit,
                      char color[4]);

/*
 * Converts 4-digit hex number represented by char[] to an integer.
 */
int convert_hex_to_int(char hex[4]);

/*
 * Converts a hex number to an integer. Can also be used to validate if a char
 * is a hex number.
 */
int get_int_from_hex(char hex);

#endif