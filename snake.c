#define _POSIX_C_SOURCE 200112L

#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <termios.h> //termios, TCSANOW, ECHO, ICANON

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"

#include "menu.h"

#define DELAY 100 * 1000 * 1000
#define RED 0xF000

int DIFFICULTY = NORMAL;
int IDX = 0;  // Index of the COLOR array
char COLOR[] = {'0', '7', 'E', '0'};

struct termios termios_setup()
{
  static struct termios oldt, newt;
  /*tcgetattr gets the parameters of the current terminal
     STDIN_FILENO will tell tcgetattr that it should write the settings
    of stdin to oldt*/
  tcgetattr(STDIN_FILENO, &oldt);
  /*now the settings will be copied*/
  newt = oldt;

  /*ICANON normally takes care that one line at a time will be processed
    that means it will return if it sees a "\n" or an EOF or an EOL*/
  newt.c_lflag &= ~(ICANON);
  newt.c_cc[VMIN] = 0; // bytes until read unblocks.
  newt.c_cc[VTIME] = 0;

  /*Those new settings will be set to STDIN
    TCSANOW tells tcsetattr to change attributes immediately. */
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);

  return oldt;
}

void menu(unsigned short *fb, unsigned char *parlcd_mem_base, unsigned char *mem_base,
          bool *exit_game, bool *play_game)
{
  bool collided, in_menu = true;
  unsigned int val_line = 1;
  int r, snake_delay_counter = 0; // Snake delay counter is used to slow down or speed up the snake
  char input, AI_input = ' ';
  unsigned char *LEDline_mem_base = mem_base + SPILED_REG_LED_LINE_o;

  snake *sn = init_snake(RED);

  struct timespec loop_delay;
  loop_delay.tv_sec = 0;
  loop_delay.tv_nsec = DELAY;

  grid_block fruit;
  fruit.x = 0;
  fruit.y = HEIGHT;
  remove_fruit(&fruit, WIDTH, HEIGHT);

  while (in_menu){
    if ((r = read(0, &input, 1)) == 1){
      in_menu = handle_menu_input(input, play_game, exit_game, &DIFFICULTY, &IDX, COLOR);
    }
    if (snake_delay_counter >= DIFFICULTY){
      AI_input = get_AI_input(fruit.x, fruit.y, sn);
    }

    collided = handle_snake(sn, &fruit, &val_line, &snake_delay_counter,
                            DIFFICULTY, AI_input, mem_base);
    if (collided){
      reset_snake(sn);
    }

    // LED line set
    *(volatile unsigned int *)(LEDline_mem_base) = (val_line << 1) - 1;

    // Rendering a frame
    create_menu_frame(fb, sn, fruit, COLOR);
    fb_to_LCD(fb, parlcd_mem_base);
    clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
  }
  free_snake(sn);
}

void gameloop(unsigned short *fb, unsigned char *parlcd_mem_base, unsigned char* mem_base)
{
  bool do_gameloop = true;
  bool collided = false;
  int r, snake_delay_counter = 0; // Snake delay counter is used to slow down or speed up the snake
  unsigned int val_line = 1;
  unsigned char *LEDline_mem_base = mem_base + SPILED_REG_LED_LINE_o;

  struct timespec loop_delay;
  loop_delay.tv_sec = 0;
  loop_delay.tv_nsec = DELAY;

  snake *sn = init_snake(convert_hex_to_int(COLOR));
  grid_block fruit;
  fruit.x = WIDTH;
  fruit.y = HEIGHT;
  remove_fruit(&fruit, WIDTH, HEIGHT);

  char input = ' ';
  while (do_gameloop && !collided){
    // Handles player input
    if (snake_delay_counter >= DIFFICULTY){
      do {
        r = read(0, &input, 1);
      } while(r == 1);
    }

    collided = handle_snake(sn, &fruit, &val_line, &snake_delay_counter,
                            DIFFICULTY, input, mem_base);

    // LED line set
    *(volatile unsigned int *)(LEDline_mem_base) = (val_line << 1) - 1;
    
    // Rendering a frame
    draw_snake_frame(fb, sn, fruit);
    fb_to_LCD(fb, parlcd_mem_base);
    clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
  }
  free_snake(sn);
}

int main(int argc, char *argv[])
{
  unsigned char *parlcd_mem_base, *mem_base;
  bool exit_game = false;
  bool play_game;

  // If mapping fails exit with error code
  parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
  if (parlcd_mem_base == NULL){
    exit(1);
  }

  mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
  if (mem_base == NULL){
    exit(1);
  }

  struct termios oldt = termios_setup();
  unsigned short *fb = init_frame_buffer();

  while (!exit_game){
    menu(fb, parlcd_mem_base, mem_base, &exit_game, &play_game);
    if (play_game){
      gameloop(fb, parlcd_mem_base, mem_base);
    }
  }

  draw_background(fb); // Resets the background when quiting
  fb_to_LCD(fb, parlcd_mem_base);

  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  free(fb);
  return 0;
}
