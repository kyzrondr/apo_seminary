#include "menu.h"

bool handle_menu_input(char input, bool *play_game, bool *exit_game,
                        int* DIFFICULTY, int* idx, char* color)
{
  if (input == 's'){
    *play_game = 1;
    return false;
  }
  else if(input == 'q'){
    *exit_game = 1;
    *play_game = 0;
    return false;
  }
  else if (input == 'n'){
    *DIFFICULTY = NORMAL;
  }
  else if (input == 'h'){
    *DIFFICULTY = HARD;
  }
  else if (get_int_from_hex(input) != -1){
    color[(*idx)] = input;
    *idx = ((*idx) + 1) % 4;
  }
  return true;
}

void draw_menu_ui(unsigned short int *fb, char color[4])
{
  int offset = 90;
  int height = 20;
  int scale = 6;
  int letter_color = 0xFFFF;

  // Green underline
  draw_line(fb, offset + 5, 110, 5, 283, 0x0F00);

  // SNAKE
  draw_char(fb, offset, height, scale, 'S', letter_color);
  draw_char(fb, offset+=60, height, scale, 'N', letter_color);
  draw_char(fb, offset+=65, height, scale, 'A', letter_color);
  draw_char(fb, offset+=55, height, scale, 'K', letter_color);
  draw_char(fb, offset+=60, height, scale, 'E', letter_color);


  offset = 160;
  height = 150;
  scale = 3;

  // Start-s
  draw_char(fb, offset, height, scale, 'S', letter_color);
  draw_char(fb, offset+=30, height, scale, 't', letter_color);
  draw_char(fb, offset+=15, height, scale, 'a', letter_color);
  draw_char(fb, offset+=25, height, scale, 'r', letter_color);
  draw_char(fb, offset+=20, height, scale, 't', letter_color);
  draw_char(fb, offset+=20, height, scale, '-', letter_color);
  draw_char(fb, offset+=20, height, scale, 's', letter_color);

  offset = 170;
  height += 50;

  // Quit-q
  draw_char(fb, offset, height, scale, 'Q', letter_color);
  draw_char(fb, offset+=30, height, scale, 'u', letter_color);
  draw_char(fb, offset+=25, height, scale, 'i', letter_color);
  draw_char(fb, offset+=15, height, scale, 't', letter_color);
  draw_char(fb, offset+=20, height, scale, '-', letter_color);
  draw_char(fb, offset+=20, height, scale, 'q', letter_color);

  offset = 360;
  height += 40;
  scale = 2;

  // Hard-h
  draw_char(fb, offset, height, scale, 'H', letter_color);
  draw_char(fb, offset+=20, height, scale, 'a', letter_color);
  draw_char(fb, offset+=15, height, scale, 'r', letter_color);
  draw_char(fb, offset+=10, height, scale, 'd', letter_color);
  draw_char(fb, offset+=20, height, scale, '-', letter_color);
  draw_char(fb, offset+=10, height, scale, 'h', letter_color);

  offset = 35;

  // Color
  draw_char(fb, offset, height, scale, 'C', letter_color);
  draw_char(fb, offset+=18, height, scale, 'o', letter_color);
  draw_char(fb, offset+=15, height, scale, 'l', letter_color);
  draw_char(fb, offset+=8, height, scale, 'o', letter_color);
  draw_char(fb, offset+=15, height, scale, 'r', letter_color);

  offset = 340;
  height += 30;

  // Normal-n
  draw_char(fb, offset, height, scale, 'N', letter_color);
  draw_char(fb, offset+=20, height, scale, 'o', letter_color);
  draw_char(fb, offset+=15, height, scale, 'r', letter_color);
  draw_char(fb, offset+=10, height, scale, 'm', letter_color);
  draw_char(fb, offset+=24, height, scale, 'a', letter_color);
  draw_char(fb, offset+=17, height, scale, 'l', letter_color);
  draw_char(fb, offset+=12, height, scale, '-', letter_color);
  draw_char(fb, offset+=10, height, scale, 'n', letter_color);

  offset = 20;
  letter_color = convert_hex_to_int(color);

  // 0x????
  draw_char(fb, offset, height, scale, '0', letter_color);
  draw_char(fb, offset+=17, height, scale, 'x', letter_color);
  draw_char(fb, offset+=17, height, scale, color[0], letter_color);
  draw_char(fb, offset+=20, height, scale, color[1], letter_color);
  draw_char(fb, offset+=20, height, scale, color[2], letter_color);
  draw_char(fb, offset+=20, height, scale, color[3], letter_color);

}

void create_menu_frame(unsigned short int *fb, snake* sn, grid_block fruit,
                      char color[4])
{
  draw_snake_frame(fb, sn, fruit);
  draw_menu_ui(fb, color);
}

int convert_hex_to_int(char hex[4]){
  int res = get_int_from_hex(hex[0])*16*16*16;
  res += get_int_from_hex(hex[1])*16*16;
  res += get_int_from_hex(hex[2])*16;
  res += get_int_from_hex(hex[3]);
  return res;
}

int get_int_from_hex(char hex){
  if (hex - '0' < 10 && hex - '0' >= 0) return hex - '0';
  else if (hex == 'A' || hex == 'a') return 10;
  else if (hex == 'B' || hex == 'b') return 11;
  else if (hex == 'C' || hex == 'c') return 12;
  else if (hex == 'D' || hex == 'd') return 13;
  else if (hex == 'E' || hex == 'e') return 14;
  else if (hex == 'F' || hex == 'f') return 15;
  else return -1;
}
