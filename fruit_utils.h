#ifndef _FRUIT_UTILS_H_
#define _FRUIT_UTILS_H_

#include "snake_struct.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define GRID_SIZE 20
#define BORDER_OFFSET 10
#define OUT_OF_BOUNDS -50
#define FRUIT_COLOR 0xFFE0
#define FRUIT_COLOR_SIX_DIGIT 0xFFFF00
#define HEX_BLACK 0x0

/*
 * Finds new random coordinates for given fruit block. 
 */
void spawn_fruit(snake* sn, grid_block* fruit, int width, int height);

/*
 * Checks the if a block1 collided with block2. 
 */
bool check_block_collision(grid_block block1, grid_block block2);

/*
 * Puts the fruit out of bounds in a way that it's furtherest away from the AI snake.
 * Resulting in the AI snake living longer.
 */
void remove_fruit(grid_block* fruit, int width, int height);

#endif
