#include "frame_buffer.h"

#define DEF_BACK_COLOR 0x0

unsigned short *init_frame_buffer()
{
  unsigned short *fb = (unsigned short *)malloc(HEIGHT * WIDTH * 2);
  if (!fb){
    fprintf(stderr, "Can't allocate snake!\n");
    exit(-1);
  }

  return fb;
}

void draw_pixel(unsigned short *fb, int x, int y, unsigned short color)
{
  if (x >= 0 && x < WIDTH && y >= 0 && y < HEIGHT){
    fb[x + WIDTH * y] = color;
  }
}

void draw_pixels_scaled(unsigned short *fb, int x, int y, int scale, unsigned short color)
{
  for (int i = 0; i < scale; i++){
    for (int j = 0; j < scale; j++){
      draw_pixel(fb, x + i, y + j, color);
    }
  }
}

void draw_background(unsigned short *fb)
{
  for (int ptr = 0; ptr < HEIGHT * WIDTH; ptr++){
    fb[ptr] = DEF_BACK_COLOR;
  }
}

void fb_to_LCD(unsigned short *fb, unsigned char *parlcd_mem_base)
{
  parlcd_write_cmd(parlcd_mem_base, 0x2c);
  for (int ptr = 0; ptr < WIDTH * HEIGHT; ptr++){
    parlcd_write_data(parlcd_mem_base, fb[ptr]);
  }
}

int char_width(font_descriptor_t *fdes, int ch)
{
  int width;
  if (!fdes->width){
    width = fdes->maxwidth;
  } else {
    width = fdes->width[ch - fdes->firstchar];
  }
  return width;
}

void draw_char(unsigned short *fb, int x, int y, int scale, char ch, unsigned short color)
{
  font_descriptor_t *fdes = &font_winFreeSystem14x16;
  int w = char_width(fdes, ch);
  const font_bits_t *ptr;
  if ((ch >= fdes->firstchar) && (ch - fdes->firstchar < fdes->size)){
    if (fdes->offset){
      ptr = &fdes->bits[fdes->offset[ch - fdes->firstchar]];
    } else {
      int bw = (fdes->maxwidth + 15) / 16;
      ptr = &fdes->bits[(ch - fdes->firstchar) * bw * fdes->height];
    }
    for (int i = 0; i < fdes->height; i++){
      font_bits_t val = *ptr;
      for (int j = 0; j < w; j++){
        if ((val & 0x8000) != 0){
          draw_pixels_scaled(fb, x + scale * j, y + scale * i, scale, color);
        }
        val <<= 1;
      }
      ptr++;
    }
  }
}

void draw_line(unsigned short *fb, int x, int y, int height, int width, unsigned short color)
{
  for (int j = 0; j < height; j++){
    for (int i = 0; i < width; i++){
      draw_pixel(fb, i + x, j + y, color);
    }
  }
}

void draw_border(unsigned short *fb)
{
  draw_line(fb,0,0,10,480,0xffff);
  draw_line(fb,0,310,10,480,0xffff);
  draw_line(fb,0,10,310,10,0xffff);
  draw_line(fb,470,10,310,480,0xffff);
}
